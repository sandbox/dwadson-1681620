<?php

/**
 * @file
 * Administration page callbacks for the authnewzware module.
 */

/**
 * Form constructor for the Authnewzware settings form.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function authnewzware_admin_settings() {
  // Get an array of Drupal roles.
  $roles = user_roles(TRUE);
  unset($roles[DRUPAL_AUTHENTICATED_RID]);

  $form['authnewzware_active_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Drupal roles for Newzware users with ACTIVE subscriptions.'),
    '#options' => $roles,
    '#default_value' => variable_get('authnewzware_active_roles', FALSE),
    '#description' => t('Newzware users who have an active subscription will be assigned to these Drupal roles.'),
  );

  $form['authnewzware_inactive_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Drupal roles for Newzware users with INACTIVE subscriptions.'),
    '#options' => $roles,
    '#default_value' => variable_get('authnewzware_inactive_roles', FALSE),
    '#description' => t("Newzware users who don't have an active subscription will be assigned to these Drupal roles."),
  );

  $form['required'] = array(
    '#title' => t('Required Newzware Authentication Parameters'),
    '#type' => 'fieldset',
    '#description' => t("Newzware's authentication page (auth70_xml.jsp) requires these parameters."),
  );
  $form['required']['authnewzware_url'] = array(
    '#title' => t('URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('authnewzware_url', 'https://subscriber.example.com/authentication/auth70_xml.jsp'),
    '#required' => TRUE,
    '#description' => t("The complete url to Newzware's authentication page (auth70_xml.jsp)."),
  );

  $form['required']['authnewzware_site'] = array(
    '#title' => t('Site Name'),
    '#type' => 'textfield',
    '#size' => 15,
    '#required' => TRUE,
    '#default_value' => variable_get('authnewzware_site', FALSE),
    '#description' => t('The site name. This is provided by a Newzware engineer and must be passed to the page.'),
  );

  $form['required']['authnewzware_name'] = array(
    '#title' => t('name'),
    '#type' => 'hidden',
    '#value' => t('authentication'),
    '#required' => TRUE,
    '#description' => t('The form name value to pass to the authentication page.'),
  );


  $form['optional'] = array(
    '#title' => t('Optional Newzware Authentication Parameters'),
    '#type' => 'fieldset',
    '#description' => t("Newzware's authentication page (auth70_xml.jsp) supports these optional parameters."),
  );

  $form['optional']['authnewzware_edition'] = array(
    '#title' => t('Edition'),
    '#type' => 'textfield',
    '#maxlength' => 3,
    '#size' => 3,
    '#default_value' => variable_get('authnewzware_edition', FALSE),
    '#description' => t('The Newzware edition code. If passed, only subscriptions to this edition will be authenticated.'),
  );

  $form['authnewzware_querybyaccount'] = array(
    '#title' => t('queryByAccount'),
    '#type' => 'hidden',
    '#value' => t('N'),
    '#required' => TRUE,
    '#description' => t('Whether to query by subscriber account instead of login and password.'),
  );

  $form['authnewzware_dom'] = array(
    '#title' => t('dom'),
    '#type' => 'hidden',
    '#value' => t('N'),
    '#required' => TRUE,
    '#description' => t('The dom parameter allows the response to be formatted in an AJAX friendly manner. If set to Y, the response returned is easily managed by the DOM in any browser.'),
  );

  return system_settings_form($form);

}
