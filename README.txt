SUMMARY
-------
The Authnewzware module authenticate users to a Newzware server.


REQUIREMENTS
------------
 * Newzware server with the Subscriber Self Service module (SSM)


INSTALLATION
------------
Install the module by following the instructions at http://drupal.org/node/70151


CONFIGURATION
-------------
Configure settings are at Administration » User Management » Newzware Authentication

Drupal Roles:
 * Active Subscription. The Drupal role(s) to assign to Newzware users who have
   a current, active subscription 
 * Inactive Subscription. The Drupal role(s) to assign to Newzware users who
   don't have a current, active subscription.
 * URL. The URL to the Newzware server's auth70_xml.jsp page.
 * Site Name. Provided by a Newzware engineer.
 * Edition (optional). If specified, only subscriptions to this edition will be
   authenticated.